testsPaths = [
  "tests/buster-test"
  "tests/couchdb-test"
  "tests/App-test"
  "tests/models/Draft-test"
  "tests/models/Comment-test"
  "tests/models/DraftAsset-test"
  "tests/controllers/DraftArrayController-test"
  "tests/controllers/DraftController-test"
  "tests/utils/validationHelpers-test"
  "tests/utils/functionalHelpers-test"
  "tests/utils/code/codeHelpers-test"
  "tests/utils/ember/controllers/CouchDBAttachmentController-test"
  "tests/utils/ember/controllers/CouchDBDocumentController-test"
  "tests/utils/ember/models/CouchDBDocument-test"
  "tests/utils/ember/models/CouchDBAttachment-test"
  "tests/utils/ember/models/serializableHelpers-test"
  "tests/utils/ember/models/dataValidators-test"
  # "tests/playground/files-test"
]
jQuery.couch.urlPrefix = '/db'

runner = buster.testRunner.create timeout: 50000
reporter = buster.reporters.html.create root:document.body
reporter.listen runner

require testsPaths, (tests...) ->
  runner.runSuite tests
