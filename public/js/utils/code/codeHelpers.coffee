define [
  "libs/code/coffee-script"
], (coffee) ->

  codeHelpers =
    coffeeToJsFile: (coffeefile, callback) ->
      reader = new FileReader()
      reader.onload = ->
        jscode = coffee.compile reader.result
        jsfile = new Blob [jscode], type:"application/javascript"
        if coffeefile.name and /.coffee$/.test coffeefile.name
          jsfile.name = coffeefile.name.replace /.coffee$/, ".js"
        callback null, jsfile
      reader.onerror = ->
        callback "error reading coffeescript file"
      reader.readAsText coffeefile
