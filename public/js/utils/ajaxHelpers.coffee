define
  sendfile: (file, url, success, error) ->
    options =
      processData: false
      data: file
      type: 'PUT'
      dataType: 'json'
      url: url
      success: success
      error: error

    options.contentType = if file.type then file.type else "text/plain;charset=UTF-8"

    $.ajax options

  deleteAsset: (url, success, error) ->
    $.ajax
      type: 'DELETE'
      dataType: 'json'
      url: url
      success: success
      error: error
