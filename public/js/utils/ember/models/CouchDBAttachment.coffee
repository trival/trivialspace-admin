define [
  "utils/ember/models/serializableHelpers"
], (serialHelpers) ->

  CouchDBAttachment = Ember.Object.extend Ember.Mixin.create(serialHelpers),
    name: ""
    path: ""
    local_file: null
    local_upload: false
    local_delete: false

  
