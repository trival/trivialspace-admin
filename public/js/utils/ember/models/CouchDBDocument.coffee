define [
  "utils/ember/models/serializableHelpers"
], (serialHelpers) ->

  CouchDBDocument = Ember.Object.extend serialHelpers,
    _id: ""
    _rev: 0
    
    getJsonId: ->
      result = @getProperties '_id', '_rev'
      throw TypeError "_id is not set or empty" unless result._id
      throw TypeError "_rev is not set or empty" unless result._rev
      return result
