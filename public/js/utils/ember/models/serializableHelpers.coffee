define ->
  serialize =
    emberObjToJsonData: (obj) ->
      unless obj instanceof Ember.Object
        throw TypeError "argument is not an Ember Object"

      emberObj = Ember.Object.create()
      props = for key, value of obj
        continue if Ember.typeOf(value) == 'function'
        continue if emberObj[key] != undefined
        continue if value == 'toString'
        continue if /^local_/i.test key
        key

      obj.getProperties props

    toJsonData: ->
      # mixin method for Ember objects
      serialize.emberObjToJsonData @

    emberObjArrToJsonDataArr: (objArray) ->
      serialize.emberObjToJsonData obj for obj in objArray

    toJsonDataArray: (arrayProperty) ->
      # mixin method for Ember objects
      serialize.emberObjArrToJsonDataArr @get arrayProperty

    dataArrayToEmberObjArray: (EmberClass, dataArray) ->
      EmberClass.create obj for obj in dataArray
