define [

  "utils/ember/models/CouchDBDocument"
  "utils/ember/models/dataValidators"
  "utils/validationHelpers"

], (Document, data, validate) ->


  (dbName) ->

    unless validate.isNotEmptyString dbName
      throw Error "the argument '#{dbName}' is invalid"

    _db = $.couch.db(dbName)

    _executeDBCallbackOperation = (funcName, obj, success, error) ->
      options = {}
      options.success = success if success
      options.error = error if error

      if options.success or options.error
        _db[funcName] obj, options
      else
        _db[funcName] obj
    

    controller =

      save: (obj, success, error) ->
        if obj instanceof Ember.Object
          throw Error "Ember.Objects must be serialized before saving"
        throw TypeError "missing _id property on obj" unless obj._id
        delete obj._rev if obj._rev == ""
        _executeDBCallbackOperation("saveDoc", obj, success, error)


      delete: (doc, success, error) ->
        throw Error "doc is not an CouchDBDocument" unless data.isCouchDBDocument doc
        obj = doc.getJsonId()
        _executeDBCallbackOperation("removeDoc", obj, success, error)


      getAll: (success) ->
        _db.allDocs success: (data) ->
          dataArray = []
          deferredArray = for row in data.rows
            $.Deferred((d) ->
              _db.openDoc row.id,
                success: (doc) ->
                  dataArray.push doc
                  d.resolve()
                error: ->
                  d.reject()
            ).promise()
          $.when.apply(null, deferredArray).then(->
            success dataArray
          ).fail(-> console.debug 'dbController.getAll failed')

      db: _db
