define
  isNotEmptyString: (param) ->
    typeof param is "string" and
      param.length > 0 and
      not /^\s*$/.test param
