  Function::partial = ->
    fn = this
    argsMem = Array::slice.call arguments
    ->
      arg = i = 0
      args = Array::slice.call argsMem
      while arg < arguments.length
        if args[i] is undefined
          args[i] = arguments[arg++]
        i++
      fn.apply this, args
