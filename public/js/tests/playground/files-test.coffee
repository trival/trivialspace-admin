define [
  'utils/ajaxHelpers'
], (ajax) ->

  doc = _id: 'files'
  db = $.couch.db('filestestdb')

  buster.testCase "js files behavior",
    setUp: (done) ->
      db.create
        success: ->
          db.saveDoc doc,
            success: (data) ->
              doc._rev = data.rev
              done()

    tearDown: ->
      db.drop()

    "a file should be uploaded to couchdb when selected": (done) ->

      input = $('<input type="file" multiple>').appendTo 'body'
      input.change ->
        file = input[0].files[0]
        console.debug file.name
        console.debug file.type
        console.debug file.size

        url = -> '/db/filestestdb/files/directory/' + file.name + '?rev=' + doc._rev
        ajax.sendfile file, url(), (data) ->
          doc._rev = data.rev
          assert data.rev, 'data.rev is not defined'
          input.remove()
          
          ajax.deleteAsset url(), (data) ->
            assert data.rev != doc._rev, "no new rev received"
            done()


      input.click()
