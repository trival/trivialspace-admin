define ->
  buster.testCase "testing couchdb",
    setUp: ->
      $.couch.login
        name: "admin"
        password: "12345"

    tearDown: ->
      $.couch.logout()

    "//basic couchdb operations": (done) ->

      $.couch.info
        success: (data) ->
          assert data, 'recieved no db info'

      $.couch.allDbs
        success: (data) ->
          assert data, 'recieved no dbs'


      db = $.couch.db('simpletestdb')

      db.create
        success: (data) ->
          assert data, 'new db not created'

          $.couch.allDbs
            success: (data) ->
              assert data.indexOf('simpletestdb') > -1, 'db is not in list of all dbs'

          db.info
            success: (data) ->
              assert data, 'no db infos received'

              db.saveDoc {
                  _id: 'newTestDoc'
                  content: 'supercontent blabla'
                },
                success: (data) ->
                  assert data, 'new document failed to create'

                  db.openDoc data.id,
                    success: (doc) ->
                      assert doc, 'document failed to open'

                      db.removeDoc doc,
                        success: (data) ->
                          assert data, 'document should be deleted'

                          db.drop
                            success: (data) ->
                              assert data, 'db should be deleted'

                              $.couch.allDbs
                                success: (data) ->
                                  assert data.indexOf('simpletestdb') is -1, 'db should be no longer accessible in couchdb'
                                  done()

    "existent trivialspace db": (done) ->
      $.couch.db('trivialspace').info
        success: (data) ->
          assert data, 'no data received from trivialspace db'
          done()
        error: ->
          assert false, 'error connecting to trivialspace db'
          done()
