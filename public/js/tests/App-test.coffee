define [
  "app/App"
], (app) ->

  buster.testCase "Ember App",
    "app is instance of Ember.Application": ->
      assert app instanceof Ember.Application

    "app is defined globally as App": ->
      assert.same app, window.App

    "has an none empty string 'DB_PREFIX' constant": ->
      prefix = app.get 'DB_PREFIX'
      app.set 'DB_PREFIX', "superdummerprefix"
      app.DB_PREFIX = "superdummerprefix"
      prefix2 = app.get 'DB_PREFIX'

      assert.defined prefix
      assert typeof prefix == "string"
      assert prefix.length > 0
      assert.equals prefix, prefix2
      refute.equals prefix2, "superdummerprefix"

    "has an none empty string 'DB_NAME' constant": ->
      dbname = app.get 'DB_NAME'

      assert.defined dbname
      assert typeof dbname == "string"
      assert dbname.length > 0
