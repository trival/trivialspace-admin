define [
  "app/models/DraftAsset"
  "utils/ember/models/CouchDBAttachment"
], (DraftAsset, CouchDBAttachment) ->

  buster.testCase "DraftAsset Model",
    setUp: ->
      @asset = DraftAsset.create()

    "should be a CouchDBAttachment": ->
      assert @asset instanceof CouchDBAttachment

    "should have properties size, local_url, local_new": ->
      assert.defined @asset.get('size'), "size undefined"
      assert.defined @asset.get('local_url'), "local_url undefined"
      assert.defined @asset.get('local_new'), "local_new undefined"
      assert.defined @asset.get('local_statusClass'), "local_statusClass undefined"
