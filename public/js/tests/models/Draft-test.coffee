define [
  "app/models/Draft"
  "utils/ember/models/CouchDBDocument"
  "app/models/DraftAsset"
], (Draft, CouchDBDocument, Asset) ->

  buster.testCase "Draft Model",
    setUp: ->
      @draft = Draft.create()

    "should have not empty string constant LOCAL_INITIAL_NAME": ->
      assert typeof @draft.get('LOCAL_INITIAL_NAME') == 'string', "should be a string"
      assert @draft.get('LOCAL_INITIAL_NAME').length > 0, "string lenght > 0"

    "should have not empty string constant LOCAL_INITIAL_ID": ->
      assert typeof @draft.get('LOCAL_INITIAL_ID') == 'string', "should be a string"
      assert @draft.get('LOCAL_INITIAL_ID').length > 0, "string lenght > 0"

    "should have array constant with all own Attachment array properties": ->
      assert.equals @draft.get('LOCAL_ATTACHMENT_KEYS'), ['code', 'models', 'textures', 'thumbs']
      
    "should be a CouchDBDocument": ->
      assert @draft instanceof CouchDBDocument

    "should have array properties for code, textures, models": ->
      assert(@draft.get('code').length == 0)
      assert(@draft.get('textures').length == 0)
      assert(@draft.get('models').length == 0)
      assert @draft.get('thumbs').length == 0

    "should have initial values for _id, name, version, lastChange": ->
      assert.equals @draft.get('_id'), @draft.get('LOCAL_INITIAL_ID')
      assert.equals @draft.get('name'), @draft.get('LOCAL_INITIAL_NAME')
      assert.equals @draft.get('version'), 0.1
      assert @draft.get('lastChange') > 0, "lastChange should be greater 0"
      assert.equals @draft.get('type'), "draft"
      assert.equals @draft.get('active'), true

    "should have properties height, width, title, mainApp, info": ->
      assert.defined @draft.get('height'), "height undefined"
      assert.defined @draft.get('width'), "width undefined"
      assert.defined @draft.get('mainApp'), "mainApp undefined"
      assert.defined @draft.get('info'), "info undefined"
      assert.defined @draft.get('thumbname'), "thumbname undefined"
