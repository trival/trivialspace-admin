define [
  "app/models/Comment"
  "utils/ember/models/CouchDBDocument"
], (Comment, CouchDBDocument) ->

  buster.testCase "Comment Model",
    setUp: ->
      @comment = Comment.create()
      
    "should be a CouchDBDocument": ->
      assert @comment instanceof CouchDBDocument

    "should have initial values for attributes": ->
      assert.equals @comment.get('type'), "comment"
      assert.equals @comment.get('active'), true
      assert.equals @comment.get('index'), 0
      assert.equals @comment.get('local_hasChanges'), false
      assert.equals @comment.get('local_delete'), false

    "should have properties" : ->
      assert.defined @comment.get('refId'), "refId undefined"
      assert.defined @comment.get('author'), "author undefined"
      assert.defined @comment.get('url'), "url undefined"
      assert.defined @comment.get('email'), "email undefined"
      assert.defined @comment.get('message'), "message undefined"
      assert.defined @comment.get('timestamp'), "timestamp undefined"
      assert.defined @comment.get('local_time'), "local_time undefined"
      assert.defined @comment.get('local_statusClass'), "local_statusClass undefined"

    "sets local_hasChanges to true on any change of data": ->
      @comment.set 'local_hasChanges', false
      @comment.set 'active', false
      assert @comment.get('local_hasChanges') == true

      @comment.set 'local_hasChanges', false
      @comment.set 'local_delete', true
      assert @comment.get('local_hasChanges') == true
