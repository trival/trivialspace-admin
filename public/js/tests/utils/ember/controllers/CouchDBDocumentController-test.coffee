define [
  "utils/ember/controllers/CouchDBDocumentController"
  "utils/ember/models/CouchDBDocument"
], (getController, Document) ->

  buster.testCase "CouchDBDocumentController",
    setUp: ->
      @db =
        saveDoc: @stub()
        removeDoc: @stub()
        allDocs: @stub()
      $.couch.db = @stub().returns @db
      @controller = getController "testdb"

    "instance is a function": ->
      assert typeof getController is "function",
        "controller should be a function"

    "throws Error unless called with not empty string argument": ->
      assert.exception => getController()
      assert.exception => getController ""
      assert.exception => getController 2000
      assert.exception => getController {}
      refute.exception => getController "hellostring"

    "should set db object with dbName argument": ->
      getController 'helloDB'

      assert.calledWith $.couch.db, 'helloDB'

    "should have a getAll method that returns an array of document from the db": ->
      docs = rows: [
        {id: 'test'}
        {id: 'test2'}
      ]
      doc1 = {_id: 'test', _rev: 2, data: 'hallo'}
      doc2 = {_id: 'test2', _rev: 4, data: 'worldo'}
      openDoc = @stub()
      openDoc.withArgs('test').yieldsTo 'success', doc1
      openDoc.withArgs('test2').yieldsTo 'success', doc2
      @db.allDocs = @stub().yieldsTo('success', docs)
      @db.openDoc = openDoc

      success = @stub()
      @controller.getAll(success)

      assert.called @db.allDocs
      assert.calledTwice openDoc
      assert.calledWith success, [doc1, doc2]

    "should have save method that":

      "throws error if obj has no _id property": ->
        assert.exception =>
          @controller.save {helmut: "kohl"}

      "saves objects to db with optional success and error callbacks": ->
        obj =
          _id: "foo"
          data: "bar"
        success = @stub()
        error = @stub()

        @controller.save obj
        @controller.save obj, success
        @controller.save obj, success, error

        assert.calledThrice @db.saveDoc
        assert.calledWith @db.saveDoc, obj
        assert.calledWith @db.saveDoc, obj, success: success
        assert.calledWith @db.saveDoc, obj,
          success: success
          error: error

      "throws an Error if obj parameter is an Ember.Object": ->
        obj = Ember.Object.create
          _id: 34
          data: 'hello'

        assert.exception => @controller.save obj

      "removes _rev before save if _rev is empty": ->
        draft =
          _id: 'testDraft'
          _name: 'test'
          _rev: ""

        @controller.save draft

        saveData = @db.saveDoc.args[0][0]
        refute.defined saveData._rev

    "should have delete method that":
      "deletes CouchDBDocuments from db": ->
        obj = Document.create
          _id: 'hans'
          name: 'hans'
          _rev: 1

        @controller.delete obj

        assert.called @db.removeDoc
        assert.equals @db.removeDoc.args[0][0], {_id: 'hans', _rev: 1}

      "accepts optional success and error callbacks": ->
        obj =
          _id: "foo"
          _rev: 5
        doc = Document.create obj
        success = @stub()
        error = @stub()

        @controller.delete doc
        @controller.delete doc, success
        @controller.delete doc, success, error

        assert.calledWith @db.removeDoc, obj
        assert.calledWith @db.removeDoc, obj, success: success
        assert.calledWith @db.removeDoc, obj,
          success: success
          error: error

      "throws an Error when argument is not a CouchDBDocument": ->
        assert.exception => @controller.delete _id: "hello", _rev: 9

      "throws exception when _id property of object is not set": ->
        assert.exception =>
          @controller.delete Document.create {name: 'hans', _rev: 1}

      "throws exception when _rev property of object is not set": ->
        assert.exception =>
          @controller.delete Document.create {_id: 'wurst'}
