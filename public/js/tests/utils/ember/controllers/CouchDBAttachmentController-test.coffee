define [
  "utils/ember/controllers/CouchDBAttachmentController"
  "utils/ember/models/CouchDBAttachment"
  "utils/ember/models/CouchDBDocument"
  "utils/ajaxHelpers"
], (getController, Attachment, Document, ajax) ->

  buster.testCase "CouchDBAttachmentgetController",
    setUp: ->
      @doc = Document.create
        _id: "superdoc"
        _rev: 30
      partialGetController = getController.partial "pathToDb"
      @controller = partialGetController @doc

    "should be a function": ->
      assert typeof getController is "function"

    "throws Error unless called with not empty string first argument": ->
      assert.exception => getController()
      assert.exception => getController "", @doc
      assert.exception => getController 2000, @doc
      assert.exception => getController {}, @doc
      refute.exception => getController "hellostring", @doc

    "the second function and the optional argument 
        should be checked to be valid CouchDBDocument": ->
      spy = @spy getController
      createFunc = spy.partial "tset"
      
      assert.exception => spy "test", Document.create _rev: 2
      assert.exception => spy "test", {_id: "superdoc", _rev: 5}
      refute.exception => spy "test", Document.create {_rev: 2, _id: "fritz"}
      assert.exception => createFunc Document.create _rev: 2
      assert.exception => createFunc {_id: "superdoc", _rev: 5}
      refute.exception => createFunc Document.create {_rev: 3, _id: "fritz"}
      refute.exception => spy "test", Ember.ObjectController.create
        content: Document.create {_rev: 2, _id: "fratz"}

    "controller returned by the factory should be unique": ->
      controller1 = getController "test", @doc
      controller2 = getController "test2", Document.create _rev: 3, _id: "testDoc"

      path1 = @controller.getAssetPath Attachment.create name: "jesus"
      path2 = controller1.getAssetPath Attachment.create name: "jesus"
      path3 = controller2.getAssetPath Attachment.create name: "jesus"

      assert.equals "pathToDb/superdoc/jesus", path1
      assert.equals "test/superdoc/jesus", path2
      assert.equals "test2/testDoc/jesus", path3

    "should have a getAssetPath method that":
      "takes a CouchDBAttachment with name set as argument or throw Error otherwise": ->
        assert.exception => @controller.getAssetPath()
        assert.exception => @controller.getAssetPath id: "ho", name: "foo"
        assert.exception => @controller.getAssetPath Attachment.create()
        refute.exception => @controller.getAssetPath Attachment.create name: "Joseph"

      "returns the db-path for this Attachment": ->
        path1 = @controller.getAssetPath Attachment.create name: "jesus"
        path2 = @controller.getAssetPath Attachment.create name: "pimmel", path: "himmel"

        assert.equals "pathToDb/superdoc/jesus", path1
        assert.equals "pathToDb/superdoc/himmel/pimmel", path2

    "should have a save method that":
      setUp: ->
        @getAssetPath = @stub(@controller, 'getAssetPath').returns "path/to/hell"
        @sendfile = ajax.sendfile = @stub()
        @asset = Attachment.create
          local_file: "superfile"
          local_upload: true
        @success = @stub()
        @error = @stub()

      "does nothing unless local_upload property of the Attachment is true": ->
        @controller.save Attachment.create()
        @controller.save "deadlock"

        refute.called @getAssetPath
        refute.called @sendfile

      "calls getAssetPath to get the right url and check argument validity": ->
        @controller.save @asset

        assert.called @getAssetPath

      "appends 'rev' getparameter to url and calls sendfile ajax-helper": ->
        @controller.save @asset

        assert.calledWith @sendfile, "superfile", "path/to/hell?rev=30"

      "calls success or error callbacks appropriately": ->
        @sendfile.callsArgWith 2, {rev: 40, ok: true}
        @controller.save @asset, @success

        @sendfile.callsArg 3
        @controller.save @asset, @success, @error

        assert.calledOnce @success
        assert.calledOnce @error

      "sets document._rev to new rev on successfull save": ->
        @sendfile.callsArgWith 2, {rev: 40, ok: true}
        
        @controller.save @asset, @success
        
        assert.equals @doc.get('_rev'), 40

      "if ok response from db is not true call error and leave _rev untouched": ->
        @sendfile.callsArgWith 2, {ok: false}

        @controller.save @asset, @success, @error

        refute.called @success
        assert.calledOnce @error
        assert.equals @doc.get('_rev'), 30

    "should have a delete method that":
      setUp: ->
        @getAssetPath = @stub(@controller, 'getAssetPath').returns "path/to/hell"
        @deleteAsset = ajax.deleteAsset = @stub()
        @asset = Attachment.create
          local_file: "superfile"
          local_delete: true
        @success = @stub()
        @error = @stub()

      "does nothing unless local_delete property of the Attachment is true": ->
        @controller.delete Attachment.create()
        @controller.delete "deadlock"

        refute.called @getAssetPath
        refute.called @deleteAsset

      "calls getAssetPath to get the right url and check argument validity": ->
        @controller.delete @asset

        assert.called @getAssetPath

      "appends 'rev' getparameter to url and calls deleteAsset ajax-helper": ->
        @controller.delete @asset

        assert.calledWith @deleteAsset, "path/to/hell?rev=30"

      "calls success or error callbacks appropriately": ->
        @deleteAsset.callsArgWith 1, {rev: 40, ok: true}
        @controller.delete @asset, @success

        @deleteAsset.callsArg 2
        @controller.delete @asset, @success, @error

        assert.calledOnce @success
        assert.calledOnce @error

      "sets document._rev to new rev on successfull delete": ->
        @deleteAsset.callsArgWith 1, {rev: 40, ok: true}
        
        @controller.delete @asset, @success
        
        assert.equals @doc.get('_rev'), 40

      "if ok response from db is not true call error and leave _rev untouched": ->
        @deleteAsset.callsArgWith 1, {ok: false}

        @controller.delete @asset, @success, @error

        refute.called @success
        assert.calledOnce @error
        assert.equals @doc.get('_rev'), 30
        
       
    "should have a syncAll method that":
      setUp: ->
        @getAssetPath = @stub(@controller, 'getAssetPath').returns "path"
        @asset1 = Attachment.create
          local_file: "superfile"
          local_upload: true
        @asset2 = Attachment.create
          local_file: "strangefile"
          local_upload: true
        @asset3 = Attachment.create
          local_file: "strangefile"
          local_upload: true
        @asset4 = Attachment.create
          local_file: "huhu i'll be gone"
          local_delete: true
        @asset5 = Attachment.create
          local_file: "i'm going up yonder"
          local_delete: true

        counter = 30
        counterajaxstub = (path, success, error)->
          for i in [1..2000000]
            i*i
          suffix = path.match(/..$/)
          if suffix[0] < counter
            error(counter)
          else
            counter += 10
            success(ok: true, rev: counter)
        ajax.sendfile = @spy (file, path, success, error) ->
          counterajaxstub path, success, error
        ajax.deleteAsset = @spy (path, success, error) ->
          counterajaxstub path, success, error
    
      "takes an array as its first argument": ->
        @controller.save = @stub()

        assert.exception => @controller.syncAll "i'm a string"
        assert.exception => @controller.syncAll 200
        refute.exception => @controller.syncAll [1,3]

      "calls save on every element that has its local_upload propertry set true": ->
        @spy @controller, "save"
        unload = Attachment.create()
        success = =>
          assert.calledTwice @controller.save
          assert.calledWith @controller.save, @asset1
          assert.calledWith @controller.save, @asset2
          
        @controller.syncAll [@asset2, @asset1, unload], success

      "takes a success and error callback": (done) ->
        @spy @controller, "save"
        @stub($,'get').callsArgWith 1, _attachment: true
        error = @stub()
        success = @spy =>
          assert.calledThrice @controller.save
          assert.calledOnce success
          assert.equals @doc.get('_rev'), 60
          refute.called error
          done()

        @controller.syncAll [@asset1, @asset2, @asset3], success, error

      "calls error callback on error": (done)->
        @spy @controller, "save"
        success = @stub()
        error = @spy =>
          assert.calledOnce error
          assert.calledOnce @controller.save
          refute.called success
          assert.equals @doc.get('_rev'), 10
          done()

        @doc.set '_rev', 10
        @controller.syncAll [@asset1, @asset2], success, error

      "calls delete on every element that has its local_delete property set true": ->
        @spy @controller, "save"
        @spy @controller, "delete"
        success = @spy =>
          assert.calledTwice @controller.save
          assert.calledTwice @controller.delete
          assert.equals @doc.get('_rev'), 70
          assert.called success

        @controller.syncAll [@asset5, @asset2, @asset4, @asset1], success

