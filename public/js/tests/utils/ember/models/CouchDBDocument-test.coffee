define [
  "utils/ember/models/CouchDBDocument"
  "utils/ember/models/serializableHelpers"
], (Document, serializableHelpers) ->

  buster.testCase "CouchDBDocument",
    setUp: ->
      @document = Document.create()

    "should have _id and _rev properties": ->
      assert.defined @document.get('_id'), "_id undefined"
      assert.defined @document.get('_rev'), "_rev undefined"

    "should be a serializableHelpers mixin": ->
      for funcName in serializableHelpers
        assert.isFunction @attachment[funcName]
      return null

    "should have a getJsonId method that":
      "extracts the _id and _rev properties from the document": ->
        doc = Document.create
          name: "lulu"
          _id: "10"
          hobby: "schwimmen"
          _rev: 5

        assert.equals doc.getJsonId(), {_id: "10", _rev: 5}

      "throws Errors when _id or _rev property is not set on obj or _id empty": ->
        doc1 = Document.create auto: "ford", _rev: "40"
        doc2 = Document.create _id: "100", auto: "skoda"
        doc3 = Document.create _id: "", _rev: "skoda"
        doc4 = Document.create _id: "100", _rev: ""

        assert.exception => doc1.getJsonId()
        assert.exception => doc2.getJsonId()
        assert.exception => doc3.getJsonId()
        assert.exception => doc4.getJsonId()

