define [
  "utils/ember/models/serializableHelpers"
], (helpers) ->

  buster.testCase "serializableHelpers",
    setUp: ->
      @data1 =
        name: "INITIAL_NAME"
        version: "0.1"
        code: []
        _rev: ""
      @model1 = Em.Object.create helpers, @data1
      @data2 = {id_local: "hellow", name: "hollow"}

      @model2 = Ember.Object.create helpers,
        name: "hollow"
        local_name: "sleepy"
        local_id: "argh"
        LOCAL_CONSTANT: "hmmpf"
        lOcAl_uGlyVariAble: 5000
        id_local: "hellow"

    "should have partial enabled": ->
      assert.defined helpers.toJsonData.partial

    "should have a toJsonData mixin method that":
      "extracts json data from Ember.Object": ->
        data = @model1.toJsonData()

        assert.equals data.name, "INITIAL_NAME"
        assert.equals data.version, '0.1'
        assert.isArray data.code
        assert.defined data._rev
        refute.defined data.isDestroyed
        refute.defined data.isDestroying

      "removes all data that starts with 'local_' or LOCAL_": ->
        data = @model2.toJsonData()

        assert.equals data, @data2

    "should have a emberObjToJsonData method that":
      "serializes a Ember Object to Json data": ->
        assert.equals @data1, helpers.emberObjToJsonData @model1

      "behaves same as toJsonData mixin method": ->
        assert.equals @model1.toJsonData(), helpers.emberObjToJsonData @model1
        assert.equals @model2.toJsonData(), helpers.emberObjToJsonData @model2

      "throw error when not given an Ember object": ->
        assert.exception -> helpers.emberObjToJsonData 1
        assert.exception -> helpers.emberObjToJsonData {hello: "kitty"}
        assert.exception -> helpers.emberObjToJsonData "piggy"

    "should have a emberObjArrToJsonDataArr method that":
      "serializes an Array of Ember Objects to an Array of Json Data objects": ->
        dataArray = helpers.emberObjArrToJsonDataArr [@model1, @model2]
        assert.equals dataArray, [@data1, @data2]

      "returns a copy and not a modified input array": ->
        modelCopy = $.extend {}, @model1
        dataArray = helpers.emberObjArrToJsonDataArr [@model1, @model2]

        refute.equals dataArray, [@model1, @model2]
        for property of modelCopy
          assert.equals modelCopy[property], @model1[property]
        null

    "should have a toJsonDataArray mixin helper that":
      "takes an key identifier to an array property and returns json data of that array": ->
        model = Ember.Object.create helpers,
          myArray: [@model1, @model2]

        data = model.toJsonDataArray "myArray"

        assert.equals data, helpers.emberObjArrToJsonDataArr [@model1, @model2]

    "should have a dataArrayToEmberObjArray method that":
      "takes an array of data and an Ember Class as arguments and returns 
          an array of Ember Objects wrapping the data": ->
        dataArray = [@data1, @data2]
        model2 = Ember.Object.create @data2
        MyClass = Ember.Object.extend
          superId: "I'm a hero!"

        objArray = helpers.dataArrayToEmberObjArray Ember.Object, dataArray
        objArray2 = helpers.dataArrayToEmberObjArray MyClass, dataArray

        for property of objArray[0]
          assert.equals objArray[0][property], @model1[property]
          assert.equals objArray[1][property], model2[property]
        assert.equals objArray2[0].get('superId'), "I'm a hero!"
        assert.equals objArray2[1].get('superId'), "I'm a hero!"


