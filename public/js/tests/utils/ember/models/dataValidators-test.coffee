define [
  "utils/ember/models/dataValidators"
], (validators) ->

  buster.testCase "dataValidators",
    "validates for CouchDBDocument": ->
      data =
        _id: null
        _rev: null
      controller = Ember.ObjectController.create
        content: data

      assert.equals validators.isCouchDBDocument(data), true
      assert.equals validators.isCouchDBDocument(controller), true

      delete data._rev

      assert.equals validators.isCouchDBDocument(data), false
      assert.equals validators.isCouchDBDocument(controller), false

    "validates for CouchDBAttachment": ->
      data =
        name: null
        path: null
        local_file: null
        local_upload: null
        local_delete: null

      assert.equals validators.isCouchDBAttachment(data), true

      delete data.local_upload

      assert.equals validators.isCouchDBAttachment(data), false
