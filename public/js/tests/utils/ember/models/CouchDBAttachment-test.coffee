define [
  "utils/ember/models/CouchDBAttachment"
  "utils/ember/models/serializableHelpers"
], (Attachment, serializableHelpers) ->

  buster.testCase "CouchDBAttachment",
    setUp: ->
      @attachment = Attachment.create()

    "should be Ember.Object": ->
      assert @attachment instanceof Ember.Object

    "should be a serializableHelpers mixin": ->
      for funcName in serializableHelpers
        assert.isFunction @attachment[funcName]
      return null

    "should have properties name, path, local_file": ->
      assert.defined @attachment.get('name'), "name undefined"
      assert.defined @attachment.get('path'), "path undefined"
      assert.defined @attachment.get('local_file'), "local_file undefined"

    "should have preset properties local_upload, local_delete": ->
      assert.equals @attachment.get('local_upload'), false
      assert.equals @attachment.get('local_delete'), false
