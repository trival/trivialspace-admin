define [
  "utils/validationHelpers"
], (validHelpers) ->

  buster.testCase "validationHelpers",
    "should have a function isNotEmptyString that returns true if parameter is not empty string, false otherwise": ->
      assert.equals false, validHelpers.isNotEmptyString("")
      assert.equals false, validHelpers.isNotEmptyString("\n  ")
      assert.equals false, validHelpers.isNotEmptyString(" ")
      assert.equals false, validHelpers.isNotEmptyString(12)
      assert.equals false, validHelpers.isNotEmptyString(null)
      assert.equals false, validHelpers.isNotEmptyString(buster)
      assert.equals true, validHelpers.isNotEmptyString("hello")
