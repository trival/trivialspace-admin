define [
  "utils/functionalHelpers"
], ->

  buster.testCase "functionalHelpers",
    "should define a partial method on Function prototype that":
      setUp: ->
        @add = @spy (a, b) -> a + b

      "exists :)": ->
        assert.defined @add.partial
        
      "returns a partial function with preset parameters": ->
        add5 = @add.partial 5
        add4 = @add.partial undefined, 4

        assert.isFunction add4
        assert.equals 7, add4 3
        assert.equals 4, @add.args[0][1]
        assert.isFunction add5
        assert.equals 8, add5 3
        assert.equals 5, @add.args[1][0]
        assert.equals 11, add5 6
        assert.equals 2, @add.args[2].length
