define [
  "utils/code/codeHelpers"
], (helpers) ->

  buster.testCase "codeHelpers",
    
    "should have a coffeeToJsFile method that compiles a coffee file to a js file": (done) ->
      coffeeCode = "hello='hello'\nalert hello"
      coffeefile = new Blob [coffeeCode], type: "text/plain"
      coffeefile.name = "test.coffee"
      reader = new FileReader()
      
      callback = (err, jsfile) ->
        assert.equals err, null
        assert.equals jsfile.name, "test.js"
        assert.equals jsfile.type, "application/javascript"
        reader.readAsText jsfile
        reader.onload = ->
          console.debug reader.result
          assert /var hello;/.test reader.result
          assert /hello\s*=\s*['"]hello['"];/.test reader.result
          assert /alert\(hello\);/.test reader.result
          done()
        reader.onerror = ->
          throw Error "coffee compile error"

      helpers.coffeeToJsFile coffeefile, callback

    
