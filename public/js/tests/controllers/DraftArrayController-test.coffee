define [
  "app/controllers/DraftArrayController"
  "app/controllers/DraftController"
  "app/models/Draft"
  "app/models/DraftAsset"
  "utils/ember/controllers/CouchDBAttachmentController"
], (Controller, DraftController, Draft, DraftAsset, getController) ->

  buster.testCase "DraftArrayController",
    setUp: ->
      @controller = Controller.create()
      @controller.set('content', [])
      @controller.set 'dbController', db: view: @stub()

    "should be Ember.ArrayController": ->
      assert @controller instanceof Ember.ArrayController

    "should have predefined properties": ->
      assert.defined @controller.get 'dbController'
      assert.defined @controller.get 'selected'
      assert.isFunction @controller.getAssetController

    "should have a createDraft method that":
      "creates new DraftController instances with unique ids": ->
        draft1 = @controller.createDraft()
        draft2 = @controller.createDraft()
        draft3 = @controller.createDraft()

        assert draft1 instanceof DraftController
        assert draft2 instanceof DraftController
        assert draft3 instanceof DraftController
        assert draft1.get('_id') !=
          draft2.get('_id') !=
          draft3.get('_id')

      "takes an optional init data argument to set initial Values for new draft": ->
        draft = @controller.createDraft
          _id: "herzschmerz"
          name: "Don Quichote"

        assert.equals draft.get('_id'), "herzschmerz"
        assert.equals draft.get('name'), "Don Quichote"

      "calls initRepresentation on the new Draft": ->
        @spy DraftController::, "initRepresentation"

        @controller.createDraft()
        @controller.createDraft _id: "fool"

        assert.calledTwice DraftController::initRepresentation

      "sets the AssetController on the new Draft": ->
        assetController = getAssetPath: @stub()
        @controller.getAssetController = @stub().returns assetController

        draft = @controller.createDraft textures: [{name: "superpig", size: 34, path: "mypath"}]

        assert.isObject draft.get 'assetController'
        assert.isFunction draft.get('assetController').getAssetPath
        assert.calledOnce assetController.getAssetPath

      "fetches comments if second param is true": ->
        @controller.set 'dbController', {db: {view: @stub()}}

        @controller.createDraft null, true

        assert.calledOnce @controller.get('dbController').db.view

    "should have a addNew method that":
      "creates new draft in controller array": ->
        @controller.addNew()

        assert @controller.get('length') == 1, "controller.length should be 2"
        assert @controller.get('lastObject') instanceof DraftController, "lastObject instanceof Draft"

      "creates unique ids for new drafts": ->
        @controller.addNew()
        @controller.addNew()
        @controller.addNew()

        assert @controller.objectAt(0).get('_id') !=
        @controller.objectAt(1).get('_id') !=
        @controller.objectAt(2).get('_id')

      "initializes new drafts": ->
        @controller.addNew()
        draft = @controller.get('firstObject')
        initvalues = draft.get 'initialValues'

        assert draft.get('hasChanges') == false
        assert.equals initvalues._id, draft.get '_id'
        assert.equals initvalues.name, draft.get 'name'
        assert.equals initvalues.version, draft.get 'version'

      "selects new Draft after creation": ->
        @controller.addNew()

        assert.equals @controller.get('selected'), @controller.objectAt(0)

    "remove draft from controller": ->
      draft = DraftController.create()
      @controller.add draft
      assert.equals @controller.indexOf(draft), 0
      @controller.remove draft
      assert.equals @controller.indexOf(draft), -1
      assert.equals @controller.get('length'), 0

    "should have an add method that adds drafts sorted by name": ->
      @controller.add Draft.create().setProperties(name: "hello")
      @controller.add Draft.create().setProperties(name: "amber")
      @controller.add Draft.create().setProperties(name: "berthold")

      assert.equals @controller.objectAt(0).name, "amber"
      assert.equals @controller.objectAt(1).name, "berthold"
      assert.equals @controller.objectAt(2).name, "hello"

     "should have a delete method that":
      setUp: ->
        @db = delete: @stub()
        @controller.set 'dbController', @db
        @draft = DraftController.create()
        @draft.destroy = @stub()
        @controller.add @draft

      "deletes a draft and removes it from db on successfull db-operation": ->
        db = delete: @stub().callsArg 1
        @controller.set 'dbController', db

        @draft.set '_rev', '1'
        @controller.delete @draft

        assert.called db.delete
        assert.called @draft.destroy
        assert.equals @controller.get('length'), 0

      "leaves the draft on unsuccessfull db-operation": ->
        @draft.set '_rev', 1
        @controller.delete @draft

        assert.called @db.delete
        refute.called @draft.destroy
        assert.equals @controller.get('length'), 1

      "removes the draft without calling db if _rev is 0": ->
        @controller.delete @draft

        refute.called @db.delete
        assert.called @draft.destroy
        assert.equals @controller.get('length'), 0

      "unsets the selected draft if it has same id": ->
        @controller.set 'selected', @draft

        @controller.delete @draft

        assert.isNull @controller.get 'selected'

      "leaves the selected draft if it has different id": ->
        selectedDraft = Draft.create().setProperties
          _id: 3
        @controller.set 'selected', selectedDraft

        @controller.delete @draft

        assert.equals @controller.get('selected'), selectedDraft

    "should have a save method that":
      setUp: ->
        @db = save: @stub()
        @controller.set 'dbController', @db
        @draft = @controller.createDraft()
        @draft.syncAssets = @spy => @draft.initRepresentation()
        @draft.setProperties
          _id: "testDraft"
          name: "Test Draft"
          lastChange: new Date()
          local_textures: [
            DraftAsset.create
              size: 34
              name: "hallo"
              path: "lele"
            DraftAsset.create
              size: 23
              name: "hello"
              path: "lala"
          ]

      "saves the draft to db, sets new initialValues, hasChanges, lastChange": ->
        draftData = @draft.getJsonData()
        draftData.textures = @draft.toJsonDataArray 'local_textures'
        spy = @spy(@draft, "getJsonData")
        @db.save.callsArgWith(1, rev: 45)

        @controller.save @draft

        assert.called spy
        assert.calledOnceWith @db.save, draftData
        assert.equals @draft.get('_rev'), 45
        assert.equals @draft.get('hasChanges'), false
        assert.equals @draft.get('initialValues')._id, 'testDraft'
        assert.equals @draft.get('initialValues').name, 'Test Draft'
        assert.equals @draft.get('initialValues')._rev, 45

      "does nothing on db error": ->
        oldDate = @draft.get 'lastChange'
        @db.save.callsArg(2)

        @controller.save @draft

        refute @draft.get '_rev'
        assert @draft.get 'hasChanges'
        assert.equals @draft.get('lastChange').toString(), oldDate.toString()

      "removes local data from drafts before sending to db": ->
        @draft.set '_id', 'testData'

        @controller.save @draft

        saveData = @db.save.args[0][0]
        refute.defined saveData.initialValues
        refute.defined saveData.hasChanges
        refute.defined saveData.local_iconClass

      "serializes its asset data before sending it to db": ->
        @controller.save @draft

        draftJson = @db.save.args[0][0]
        assert.equals draftJson.textures, [{size: 34, name: "hallo", path: "lele"},
            {size: 23, name: "hello", path: "lala"}]

      "calls syncAssets on successfull save": ->
        @db.save.callsArgWith(1, rev: 34)

        @controller.save @draft

        assert.calledOnce @draft.syncAssets

    "should have a reset method that restores draft data from draft initialValues": ->
      @controller.addNew()
      draft = @controller.get('firstObject')
      initialValues = draft.get 'initialValues'

      draft.set 'name', 'super draft'
      draft.set 'width', 1234
      draft.set '_id', 'helloId'
      @controller.reset draft

      assert.equals draft.get('name'), initialValues.name
      assert.equals draft.get('width'), initialValues.width
      assert.equals draft.get('_id'), initialValues._id
      assert.equals draft.get('hasChanges'), false
      assert.equals draft.get('hasCommentChanges'), false

    "should have a uploadChanges method that saves all docs that have changes": ->
      @controller.addNew()
      @controller.addNew()
      @controller.addNew()
      first = @controller.get 'firstObject'
      last = @controller.get 'lastObject'
      first.set '_id', 'newID'
      last.set 'width', 100
      db =
        save: @stub().callsArgWith(1, rev: 2)
      @controller.set 'dbController', db
      stub = @stub(DraftController::, 'syncAssets')

      @controller.uploadChanges()

      assert.calledTwice db.save
      assert.equals first.get('_rev'), 2
      assert.equals last.get('_rev'), 2

      stub.restore()

    "should have a getRemote method that":
      setUp: ->
        @data = rows: [
          {value: {_id: 'test', _rev: '2-asdf', name: 'hallo'}}
          {value: {_id: 'test2', _rev: '4-qwertz', name: 'worldo'}}
        ]
        @controller.set 'dbController', db: view: @stub().yieldsTo 'success', @data

      "adds remote drafts to list": ->
        @controller.getRemote()

        first = @controller.get('firstObject')
        last = @controller.get('lastObject')
        assert last instanceof DraftController, 'last instanceof Draft'
        assert first instanceof DraftController, 'first instanceof Draft'
        assert.equals first.get('name'), 'hallo'
        assert.equals last.get('_id'), 'test2'
        assert.equals last.get('_rev'), '4-qwertz'
        assert.equals first.get('hasChanges'), false
        initData = Draft.create(@data.rows[0].value).toJsonData()
        assert.equals first.get('initialValues'), initData

      "not adds a remote draft if controller contains draft with same _id and same or newer _rev": ->
        firstdraft1 = DraftController.create
          _id: 'test', _rev: '2-asdf', name: 'old1'
        firstdraft2 = DraftController.create
          _id: 'test2', _rev: '5-asdf', name: 'old2'
        @controller.add firstdraft1
        @controller.add firstdraft2

        @controller.getRemote()

        assert.equals @controller.get('length'), 2
        assert.equals @controller.get('firstObject'), firstdraft1
        assert.equals @controller.get('lastObject'), firstdraft2

      "replaces the the old draft with a new draft with same _id and newer _rev": ->
        firstdraft = DraftController.create().setProperties
          _id: 'test2', _rev: '3-zioisdfdf', name: 'alicia'
        @controller.add firstdraft

        @controller.getRemote()

        assert.equals @controller.get('length'), 2
        assert.equals @controller.get('firstObject.name'), 'hallo'
        assert.equals @controller.get('lastObject.name'), 'worldo'

    "should have a selectAssetFile method that":
      setUp: ->
        changeFunction = @stub()
        @files = [{name: "hello0", type: "type0", size: "200"}]
        @inputMock =
          change: changeFunction
          unbind: @stub()
          click: -> changeFunction.yield()
          "0": files: @files
        @stub(window, '$').withArgs("#file-selector").returns @inputMock
        @controller.set 'selected', DraftController.create()
        @stub @controller.get('selected'), 'updateAsset'

      "takes an type argument that needs to be 'textures', 'code' or 'models'": ->
        assert.exception => @controller.selectAssetFile "Ralf"
        assert.exception => @controller.selectAssetFile ""
        refute.exception => @controller.selectAssetFile Draft::get('LOCAL_ATTACHMENT_KEYS')[0]
        refute.exception => @controller.selectAssetFile Draft::get('LOCAL_ATTACHMENT_KEYS')[1]
        refute.exception => @controller.selectAssetFile Draft::get('LOCAL_ATTACHMENT_KEYS')[2]

      "check if controller.selected is set with": ->
        @controller.set('selected', null)

        assert.exception => @controller.updateAsset @file, "textures"

      "retrieves the file-input, sets its change-event and": ->
        @controller.selectAssetFile "textures"

        assert.calledWith $, "#file-selector"
        assert.called @inputMock.change
        assert.calledWith @inputMock.unbind, 'change'
        assert.calledWith @controller.get('selected').updateAsset, @files[0], "textures"

