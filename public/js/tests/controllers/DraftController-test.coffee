define [
  "app/controllers/DraftController"
  "app/models/Draft"
  "app/models/DraftAsset"
  "utils/ember/controllers/CouchDBAttachmentController"
], (Controller, Draft, DraftAsset, getController) ->

  buster.testCase "DraftController",
    setUp: ->
      @controller = Controller.create()

    "should be Ember.ArrayController": ->
      assert @controller instanceof Ember.ObjectController

    "should have predefined properties": ->
      assert.same @controller.get('hasChanges'), false
      assert.same @controller.get('hasCommentChanges'), false
      assert.defined @controller.get('assetController'), "assetController undefined"
      assert.defined @controller.get('iconClass'), "iconClass undefined"
      assert.defined @controller.get('timeString'), "timeString undefined"
      assert.defined @controller.get('initialValues'), "initialValues undefined"
      assert.defined @controller.get('local_textures'), "local_textures undefined"
      assert.defined @controller.get('local_models'), "local_models undefined"
      assert.defined @controller.get('local_code'), "local_code undefined"
      assert.defined @controller.get('local_thumbs'), "local_thumbs undefined"
      assert.defined @controller.get('local_comments'), "local_comments undefined"


    "sets hasChanges to true on any change of data": ->
      @controller.set 'hasChanges', false
      @controller.set '_id', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.set '_rev', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.set 'mainApp', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.set 'active', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.set 'lastChange', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.set 'name', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.set 'version', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.set 'width', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.set 'height', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.set 'info', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.set 'thumbname', 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.get('local_code').pushObject 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.get('local_models').pushObject 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.get('local_textures').pushObject 'superId'
      assert @controller.get('hasChanges') == true

      @controller.set 'hasChanges', false
      @controller.get('local_thumbs').pushObject 'superId'
      assert @controller.get('hasChanges') == true

    "should have an toAssetArray Method that
        creates a Attachment Object array from an own array property": ->
      @controller.set 'code', [{size: 2, name: "hello"}, {size: 5, name: "world"}]

      assets = @controller.toAssetArray 'code'

      assert assets[0] instanceof DraftAsset
      assert assets[1] instanceof DraftAsset
      assert.equals 2, assets[0].get 'size'
      assert.equals "world", assets[1].get 'name'

    "should have an initAssets method that":
      "refreshes the contents of local_textures, local_models etc. with content of code...": ->
        @controller.set 'code', [{size: 2, name: "hello"}, {size: 5, name: "world"}]
        @controller.set 'textures', [name: "mont blanc", path: "black"]

        @controller.initAssets()

        localCode = @controller.get 'local_code'
        assert.equals localCode.length, 2
        assert localCode[0] instanceof DraftAsset
        assert.equals localCode[1].get('size'), 5
        assert.equals @controller.get('local_textures').length, 1

      "sets the local_url on all textures": ->
        controller = getController "db", @controller.get 'content'
        @controller.set 'assetController', controller
        @controller.set '_id', "superDraft"
        @controller.set 'textures', [name: "pic1.jpg", path: "textures", size: 345]
        @controller.initRepresentation()

        assert.equals @controller.get('local_textures')[0].get('local_url'),
            'db/superDraft/textures/pic1.jpg'

    "should have a getJsonId method that calls toJsonData on content": ->
      @controller.set '_rev', 234
      data = @controller.getJsonId()

      assert.equals data, @controller.get('content').getJsonId()

    "should have a getJsonData method that":
      "calls toJsonData on content and removes empty rev": ->
        data = @controller.getJsonData()
        comparedata = @controller.get('content').toJsonData()
        delete comparedata._rev

        assert.equals data, comparedata

    "should have a initRepresentation method that":
      setUp: ->
        @draftData = Draft.create(
          _id: "superDraft"
          _rev: 3
          name: "Super Draft"
          textures: [
            {name: "pic1.jpg", path: "textures", size: 345}
            {name: "pic2.jpg", path: "textures", size: 987}
          ]
          code: [
            {name: "supercode.js", path: "code", size: 123}
          ]
        ).toJsonData()
        @draft = Controller.create().setProperties @draftData

      "updates initialValues to current data status": ->
        @draft.initRepresentation()

        assert.equals @draft.get('initialValues'), @draftData

        @draft.set 'name', "Awsome Draft"
        @draft.initRepresentation()

        @draftData.name = "Awsome Draft"
        assert.equals @draft.get('initialValues'), @draftData

      "initializes local assets properties": ->
        @draft.initRepresentation()

        for texture in @draft.get 'local_textures'
          assert texture instanceof DraftAsset
          assert.equals texture.get('path'), "textures"

        assert.equals @draft.get('local_textures').length, 2
        assert.equals @draft.get('local_code').length, 1

      "resets hasChanges": ->
        @draft.initRepresentation()

        assert.equals @draft.get('hasChanges'), false

    "should have a updateAsset method that":
      setUp: ->
        @file =
          size: 200
          type: "image/jpeg"
          name: "hello.jpg"
        @file2 =
          size: 400
          type: "text/plain"
          name: "juhu.txt"
        @controller = Controller.create().setProperties
          local_code: []
          local_textures: []
          local_models: []
        @stub(URL, "createObjectURL").returns "newUrl"

      "must set one of Draft::LOCAL_ATTACHMENT_KEYS as second argument": ->
        assert.exception => @controller.updateAsset @file, "Ralf"
        assert.exception => @controller.updateAsset @file, ""
        refute.exception => @controller.updateAsset @file, Draft::get('LOCAL_ATTACHMENT_KEYS')[0]
        refute.exception => @controller.updateAsset @file, Draft::get('LOCAL_ATTACHMENT_KEYS')[1]
        refute.exception => @controller.updateAsset @file, Draft::get('LOCAL_ATTACHMENT_KEYS')[2]

      "takes a file as first argument": ->
        assert.exception => @controller.updateAsset {id: "lolo", name: "wurst"}, "textures"
        refute.exception => @controller.updateAsset @file, "textures"

      "checks if type is png or jpeg for 'textures' target": ->
        img1 = $.extend({}, @file)
        img2 = $.extend({}, @file)
        img3 = $.extend({}, @file)
        img1.type = "image/tiff"
        img2.type = "hellloo"
        img3.type = "text/plain"

        assert.exception => @controller.updateAsset img1, "textures"
        assert.exception => @controller.updateAsset img2, "textures"
        assert.exception => @controller.updateAsset img3, "textures"

      "adds a new DraftAsset to target on selected Draft": ->
        @controller.updateAsset @file, "textures"
        @controller.updateAsset @file2, "code"

        code = @controller.get('local_code')
        textures = @controller.get('local_textures')
        assert.equals code.length, 1
        assert code[0] instanceof DraftAsset
        assert textures[0] instanceof DraftAsset
        assert.equals textures.length, 1
        assert.equals textures[0].get('size'), @file.size
        assert.equals textures[0].get('name'), @file.name
        assert.equals textures[0].get('path'), "textures"
        assert.equals textures[0].get('local_file'), @file
        assert.equals textures[0].get('local_upload'), true
        assert.equals textures[0].get('local_new'), true
        assert.equals code[0].get('size'), @file2.size
        assert.equals code[0].get('name'), @file2.name
        assert.equals code[0].get('path'), "code"
        assert.equals code[0].get('local_file'), @file2
        assert.equals code[0].get('local_upload'), true

      "creates a new local url for assets": ->
        @controller.updateAsset @file, "textures"

        assert.called URL.createObjectURL
        assert.equals @controller.get('local_textures')[0].get('local_url'), 'newUrl'

      "replaces assets with same name": ->
        file =
          size: 300
          type: "image/jpeg"
          name: "hello.jpg"

        @controller.updateAsset @file, "textures"
        @controller.get('local_textures')[0].set 'local_new', false
        @controller.updateAsset file, "textures"

        assert.equals 1, @controller.get('local_textures.length')
        assert.equals @controller.get('local_textures')[0].get('size'), 300
        assert.equals @controller.get('local_textures')[0].get('local_new'), false

    "should have a updateAssetsData method that":
      "saves all local assetArray to the data Arrays": ->
        draft = Controller.create().setProperties
          local_textures: [
            DraftAsset.create local_file: "boo", size: "lala", name: "foo"
            DraftAsset.create local_file: "boo2", size: "lala2", name: "foo2"
          ]
          local_code: [
            DraftAsset.create local_file: "boo3", size: "lala3", name: "foo3"
          ]
          local_models: []

        draft.updateAssetsData()

        assert.equals draft.get('textures'), [{size: "lala", path: "", name: "foo"},
          {size: "lala2", path: "", name: "foo2"}]
        assert.equals draft.get('code'), [{size: "lala3", path:"", name: "foo3"}]
        assert.equals draft.get('models'), []

      "lets out all Assets that are to be deleted": ->
        draft = Controller.create().setProperties
          local_models: [
            DraftAsset.create name: "bla", local_delete: true
            DraftAsset.create name: "hero", local_upload: true
          ]
          local_textures: []
          local_code: []

        draft.updateAssetsData()

        assert.equals draft.get('models'), [name: "hero", path: "", size: ""]

    "should have a syncAssets method that":
      setUp: ->
        @draft = Controller.create().setProperties
          local_textures: ["fu", "man", "chu"]
          local_code: ["foo", "bar"]
          local_models: ["test"]
        @assetController = syncAll: @stub()
        @draft.set 'assetController', @assetController

      "calls assetController.syncAll on all local asset arrays": ->
        @draft.syncAssets()

        assert.calledWith @assetController.syncAll, ["foo", "bar", "test", "fu", "man", "chu"]

      "after success":
        setUp: ->
          @assetController.syncAll.callsArg 1

        "deletes all assets with set local_delete tag": ->

        "stores all changes to local asset to data model": ->
        "refreshes representation": ->
          @draft.initRepresentation = @stub()

          @draft.syncAssets()

          assert.called @draft.initRepresentation
