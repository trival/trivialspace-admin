require [
  "app/controllers/DraftArrayController"
  "utils/ember/controllers/CouchDBDocumentController"
  "utils/ember/controllers/CouchDBAttachmentController"
  "app/views/SidebarView"
  "app/views/MaincontentView"
  "app/App"
], (DraftController, getDBController, getAttachmentController, SidebarView, MaincontentView, App) ->

  $.couch.urlPrefix = App.get 'DB_PREFIX'

  dbController = getDBController App.get 'DB_NAME'
  getAssetController =
    getAttachmentController.partial App.get 'DB_PATH'

  draftController = DraftController.create
    dbController: dbController
    getAssetController: getAssetController
  App.draftController = draftController

  sidebarView = SidebarView.create
    controller: draftController
  sidebarView.appendTo '#sidebar'

  maincontentView = MaincontentView.create
    controller: draftController
  maincontentView.appendTo '#maincontent'
