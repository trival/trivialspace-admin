define [

  "app/models/Draft"
  "app/models/DraftAsset"
  "utils/ember/models/serializableHelpers"
  "libs/code/coffee-script"

], (Draft, DraftAsset, serialize, coffee) ->


  DraftController = Ember.ObjectController.extend serialize,

    content: null

    hasChanges: true
    hasCommentChanges: false
    initialValues: null

    local_textures: []
    local_models: []
    local_code: []
    local_thumbs: []
    local_screenshots: []

    local_comments: []

    assetController: null

    iconClass: (->
      if @get('hasChanges') or @get 'hasCommentChanges'
        "icon-share"
      else if @get '_rev'
        "icon-check"
      else
        "icon-edit"
    ).property 'hasChanges', '_rev', 'hasCommentChanges'

    timeString: (->
      new Date(parseInt @get 'lastChange').toUTCString()
    ).property 'lastChange'

    changesObserver: (->
      @set 'hasChanges', true
    ).observes('_id', '_rev', 'active', 'name', 'mainApp', 'navigation', 'sourcelink'
      'width', 'height', 'version', 'info', 'lastChange', 'thumbname', 'screenshot',
      'local_code.@each', 'local_textures.@each',
      'local_thumbs.@each', 'local_models.@each', 'local_screenshots.@each')

    init: ->
      @set 'content', Draft.create()
      @set 'hasChanges', false

    toAssetArray: (arrayProperty) ->
      @dataArrayToEmberObjArray DraftAsset, @get arrayProperty

    getJsonId: ->
      @get('content').getJsonId()

    getJsonData: ->
      @set 'lastChange', parseInt @get 'lastChange'
      data = @get('content').toJsonData()
      delete data._rev unless data._rev
      data

    initAssets: ->
      for assetKey in @get 'LOCAL_ATTACHMENT_KEYS'
        @set "local_#{assetKey}", @toAssetArray assetKey
        if @get('assetController')
          for asset in @get "local_#{assetKey}"
            asset.set 'local_url', @get('assetController').getAssetPath asset

    initRepresentation: ->
      @set 'initialValues', @getJsonData()
      @initAssets()
      @set 'hasChanges', false

    updateAsset: (file, target) ->
      unless file and file.name and file.type? and file.size
        throw TypeError "no correct file was given in parameter"

      if Draft::get('LOCAL_ATTACHMENT_KEYS').indexOf(target) == -1
        throw TypeError "updateAsset: wrong target parameter"

      if target == 'textures' and ["image/jpeg", "image/png"].indexOf(file.type) == -1
        throw Error "file must be an Jpeg or Png image"

      old = @get("local_#{target}").filterProperty 'name', file.name
      @get("local_#{target}").removeObject asset for asset in old

      asset = DraftAsset.create
        name: file.name
        size: file.size
        path: target
        local_file: file
        local_upload: true
        local_url: URL.createObjectURL(file)

      asset.set 'local_new', true unless old.length > 0 and not old[0].get 'local_new'

      @get("local_#{target}").pushObject asset

    updateAssetsData: ->
      for key in @get 'LOCAL_ATTACHMENT_KEYS'
        assets = @get("local_#{key}").filter (asset) ->
          asset.get('local_delete') isnt true
        @set key, @emberObjArrToJsonDataArr assets

    syncAssets: ->
      assets = []
      assets.addObjects @get "local_#{key}" for key in @get 'LOCAL_ATTACHMENT_KEYS'

      success = =>
        @initRepresentation()

      @get('assetController').syncAll assets, success

    compileCoffeeCode: (asset) ->
      unless /.coffee$/.test asset.name
        throw Error "not a coffee-script file"

      createJs = (code) =>
        jscode = coffee.compile code
        jsfile = new Blob [jscode], type: "application/javascript"
        jsfile.name = asset.name.replace /.coffee$/, ".js"
        @updateAsset jsfile, 'code'

      if asset.get 'local_file'
        reader = new FileReader()
        reader.readAsText asset.get 'local_file'
        reader.onload = ->
          createJs reader.result
      else
        $.get asset.get('local_url'), createJs, 'text'

