define [
  'app/models/Draft'
  'app/models/Comment'
  'app/models/DraftAsset'
  "app/controllers/DraftController"
], (Draft, Comment, DraftAsset, DraftController) ->

  _newDraftCounter = 0

  DraftArrayController = Em.ArrayController.extend
    content: []

    dbController: null
    selected: null

    getAssetController: -> null

    createDraft: (initialData, fetchComments) ->
      draft = if initialData?
          DraftController.create().setProperties initialData
        else
          d = DraftController.create()
          d.set '_id', d.get('_id') + _newDraftCounter if _newDraftCounter > 0
          _newDraftCounter += 1
          d
      draft.set 'assetController', @getAssetController draft
      draft.initRepresentation()
      if fetchComments
        @initComments draft
      return draft

    addNew: ->
      draft = @createDraft()
      @add draft
      @set 'selected', draft

    add: (draft) ->
      idx = @binarySearch draft.get('name'), 0, @get 'length'
      @insertAt idx, draft

    binarySearch: (value, low, high) ->
      return low if low == high

      mid = low + Math.floor((high - low) / 2)
      midValue = @objectAt(mid).get 'name'

      if value > midValue
        @binarySearch value, mid+1, high
      else if value < midValue
        @binarySearch value, low, mid
      else
        mid

    remove: (draft) ->
      @removeObject draft

    delete: (draft) ->
      success = =>
        @remove draft
        @set 'selected', null if @get('selected._id') == draft.get '_id'
        draft.destroy()

      if draft.get '_rev'
        @get('dbController').delete draft, success
      else
        success()

    save: (draft) ->
      db = @get 'dbController'
      oldValues = draft.getJsonData()
      draft.updateAssetsData()

      success = (data) ->
        draft.set '_rev', data.rev
        draft.syncAssets()

      error = ->
        draft.setProperties oldValues

      db.save draft.getJsonData(), success, error

    reset: (draft) ->
      draft.setProperties draft.get 'initialValues' if draft.get 'initialValues'
      draft.initRepresentation()
      @initComments draft
      draft.set 'hasChanges', false
      draft.set 'hasCommentChanges', false

    uploadChanges: ->
      for draft in @get 'content'
        @save draft if draft.get 'hasChanges'
        if draft.get 'hasCommentChanges'
          db = @get 'dbController'
          for comment in draft.get 'local_comments'
            do (comment) ->
              if comment.get 'local_delete'
                db.delete comment, ->
                  draft.get('local_comments').removeObject comment
              else if comment.get 'local_hasChanges'
                db.save comment.toJsonData()
          draft.set 'hasCommentChanges', false

    getRemote: ->
      @get('dbController').db.view 'views/draftsByID',
        success: (results) =>
          for result in results.rows
            item = result.value
            unless (@find (current) ->
              true if current.get('_id') == item._id and current.get('_rev') >= item._rev
            )
              old = @find (current) ->
                true if current.get('_id') == item._id and current.get('_rev') < item._rev
              @remove old if old?
              @add @createDraft item, true
        error: ->
          console.debug "failed to load remote drafts"

    initComments: (draft) ->
      draft.set 'local_comments', []
      @get('dbController').db.view 'views/commentsAll',
        startkey: [draft.get '_id']
        endkey: [draft.get('_id'), {}]
        success: (results) ->
          for result, index in results.rows
            comment = Comment.create result.value
            comment.set 'index', index + 1
            draft.get('local_comments').pushObject comment

    selectAssetFile: (target) ->
      if Draft::get('LOCAL_ATTACHMENT_KEYS').indexOf(target) == -1
        throw TypeError "selectAssetFile: wrong target parameter"

      unless @get('selected') instanceof DraftController
        throw Error "there is no Draft currently selected"

      input = $("#file-selector")
      input.unbind 'change'
      input.change =>
        console.debug input[0].files
        file = input[0].files[0]
        @get('selected').updateAsset(file, target) if file

      input.click()

