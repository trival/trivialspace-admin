define [
  "utils/ember/models/CouchDBDocument"
], (CouchDBDocument) ->

  _type = "draft"
  _initialName = "new draft"
  _initialId = 'newDraft'
  _attachmentKeys = ['code', 'models', 'textures', 'thumbs', 'screenshots']

  Draft = CouchDBDocument.extend

    LOCAL_INITIAL_ID: (-> _initialId).property()
    LOCAL_INITIAL_NAME: (-> _initialName).property()
    LOCAL_ATTACHMENT_KEYS: (-> _attachmentKeys).property()

    _id: _initialId

    type: (-> _type).property()
    active: true

    name: _initialName
    mainApp: ""
    width: ""
    height: ""
    version: "0.1"
    navigation: ""
    info: ""
    thumbname: ""
    screenshot: ""
    sourcelink: ""
    lastChange: new Date().getTime()

    code: []
    textures: []
    models: []
    thumbs: []
    screenshots: []
