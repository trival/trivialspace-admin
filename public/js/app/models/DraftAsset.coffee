define [
  "utils/ember/models/CouchDBAttachment"
], (CouchDBAttachment)->
  
  DraftAsset = CouchDBAttachment.extend
    size: ""
    local_url: null
    local_new: false
    local_statusClass: (->
      if @get 'local_delete'
        "icon-trash"
      else if @get('local_upload') and @get('local_new')
        "icon-upload"
      else if @get('local_upload') and not @get('local_new')
        "icon-refresh"
      else
        "icon-ok"
    ).property 'local_delete', 'local_new', 'local_upload'
