define [
  "utils/ember/models/CouchDBDocument"
], (CouchDBDocument) ->

  _type = "comment"

  Comment = CouchDBDocument.extend

    type: (-> _type).property()
    active: true

    refId: ""
    author: ""
    url: ""
    email: ""
    message: ""
    timestamp: ""
    index: 0

    local_hasChanges: false
    local_delete: false

    local_time: (->
      new Date(@get 'timestamp').toString()
    ).property 'timestamp'
    
    local_statusClass: (->
      if @get 'local_delete'
        "icon-trash"
      else if @get 'local_hasChanges'
        "icon-refresh"
      else
        "icon-ok"
    ).property 'local_delete', 'local_hasChanges'
    
    changesObserver: (->
      @set 'local_hasChanges', true
    ).observes 'active', 'local_delete'
