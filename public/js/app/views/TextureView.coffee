define [
  "text!./templates/textureview.hbs"
], (textureTpl) ->

  TextureView = Ember.View.extend
    template: Ember.Handlebars.compile textureTpl
    tagName: 'li'
    classNames: ['thumbbox']

    delete: ->
      @set 'texture.local_delete', true
      @set 'controller.selected.hasChanges', true

    undoDelete: ->
      @set 'texture.local_delete', false
