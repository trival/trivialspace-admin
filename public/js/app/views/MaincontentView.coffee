define [

  "./TextureView"
  "text!./templates/maincontent.hbs"
  "./CodeView"
  "./ModelView"
  "./ThumbView"
  "./ScreenshotView"
  "libs/bootstrap/bootstrap-tab"
  "libs/bootstrap/bootstrap-dropdown"

], (TextureView, maincontentTpl, CodeView, ModelView, ThumbView, ScreenshotView) ->


  MaincontentView = Ember.View.extend
    template: Ember.Handlebars.compile maincontentTpl
    textureView: TextureView
    codeView: CodeView
    modelView: ModelView
    thumbView: ThumbView
    screenshotView: ScreenshotView

    mouseEnter: ->
      console.debug "maincontent mouseEnter!!!"
      setTimeout (-> $('#lastChange-input').datepicker dateFormat: '@'), 200
    
    deleteSelected: ->
      @get('controller').delete @get 'controller.selected'

    resetSelected: ->
      @get('controller').reset @get 'controller.selected'

    selectTexture: ->
      @get('controller').selectAssetFile "textures"

    selectCode: ->
      @get('controller').selectAssetFile "code"

    selectModel: ->
      @get('controller').selectAssetFile "models"

    selectThumb: ->
      @get('controller').selectAssetFile "thumbs"

    selectScreenshot: ->
      @get('controller').selectAssetFile "screenshots"

    updateTime: ->  
      @set 'controller.selected.lastChange', new Date().getTime()

    toggleActive: ->
      @set 'controller.selected.active', not @get 'controller.selected.active'
