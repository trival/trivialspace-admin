define [
  "text!./templates/thumbview.hbs"
], (thumbsTpl) ->

  TextureView = Ember.View.extend
    template: Ember.Handlebars.compile thumbsTpl
    tagName: 'div'
    classNames: ['thumb-view']

    mainThumb: (->
      @get('controller.selected.thumbname') is @get 'thumb.name'
    ).property 'controller.selected.thumbname', 'thumb.name'

    delete: ->
      @set 'thumb.local_delete', true
      @set 'controller.selected.hasChanges', true

    undoDelete: ->
      @set 'thumb.local_delete', false
      
    select: ->
      @set 'controller.selected.thumbname', @get 'thumb.name'
