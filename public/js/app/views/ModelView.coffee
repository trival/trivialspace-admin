define [
  "text!./templates/modelview.hbs"
], (template)->

  ModelView = Ember.View.extend
    template: Ember.Handlebars.compile template
    tagName: 'tr'
    selectedBinding: 'App.draftController.selected'

    delete: ->
      @set 'model.local_delete', true
      @set 'selected.hasChanges', true

    undoDelete: ->
      @set 'model.local_delete', false

