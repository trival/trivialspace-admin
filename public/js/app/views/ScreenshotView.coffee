define [
  "text!./templates/screenshotview.hbs"
], (screenshotsTpl) ->

  ScreenshotView = Ember.View.extend
    template: Ember.Handlebars.compile screenshotsTpl
    tagName: 'div'
    classNames: ['thumb-view']

    mainScreenshot: (->
      @get('controller.selected.screenshot') is @get 'screenshot.name'
    ).property 'controller.selected.screenshot', 'screenshot.name'

    delete: ->
      @set 'screenshot.local_delete', true
      @set 'controller.selected.hasChanges', true

    undoDelete: ->
      @set 'screenshot.local_delete', false
      
    select: ->
      @set 'controller.selected.screenshot', @get 'screenshot.name'
