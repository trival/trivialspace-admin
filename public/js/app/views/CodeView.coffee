define [
  "text!./templates/codeview.hbs"
  "utils/code/codeHelpers"
], (template, codeHelpers) ->

  CodeView = Ember.View.extend
    template: Ember.Handlebars.compile template
    tagName: 'tr'
    selectedBinding: 'App.draftController.selected'

    mainAppLabel: (->
      @get('selected.mainApp') is @get 'code.name'
    ).property 'selected.mainApp', 'code.name'

    isCoffeeCode: (->
      /.coffee$/.test @get 'code.name'
    ).property 'code.name'

    isJsCode: (->
      /.js$/.test @get 'code.name'
    ).property 'code.name'

    chooseMainApp: ->
      @set 'selected.mainApp', @get 'code.name'

    compileCoffee: ->
      @get('selected').compileCoffeeCode @get 'code'

    minifyCode: ->

    delete: ->
      @set 'code.local_delete', true
      @set 'selected.hasChanges', true

    undoDelete: ->
      @set 'code.local_delete', false
