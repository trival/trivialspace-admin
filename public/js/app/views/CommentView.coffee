define [
  "text!./templates/commentview.hbs"
], (template) ->

  CommentView = Ember.View.extend
    template: Ember.Handlebars.compile template

    delete: ->
      @set 'comment.local_delete', true
      @set 'controller.selected.hasCommentChanges', true

    undoDelete: ->
      @set 'comment.local_delete', false

    toggleActive: ->
      @set 'comment.active', not @get 'comment.active'
      @set 'controller.selected.hasCommentChanges', true
