define [
  "text!./templates/sidebar.hbs"
], (sidebarTpl) ->

  SidebarView = Ember.View.extend
    template: Ember.Handlebars.compile sidebarTpl
    listItem: Ember.View.extend
      tagName: 'li'
      classNameBindings: ['active']
      selectedBinding: 'App.draftController.selected'
      active: (->
          @get('draft._id') == @get('selected._id')
        ).property('draft', 'selected')
      click: (event) ->
        @set 'selected', @get 'draft'
