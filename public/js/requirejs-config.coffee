require.config
  baseUrl: '/js'
  paths:
    order: 'libs/require-order'
    text: 'libs/require-text'
    ui: 'libs/jquery-ui/minified'
  waitSeconds: 15

