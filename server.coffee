express = require 'express'
assets = require 'connect-assets'
request = require 'request'
config = require "./server/server-config"

app = express()

app.configure ->
  app.use app.router
  app.use assets(src: config.CLIENT_DIR, build: false)
  app.use express.static(config.CLIENT_DIR)
  app.use express.errorHandler( dumpExceptions: true, showStack: true )


# proxy to couchdb
app.all /^\/db(\/.*)$/, (req, res) ->
  query = require('url').parse(req.url, true).search
  url = config.DB_URL + req.params[0] + query

  console.log '------- db-query ------- \nmethod: ' + req.method
  console.log 'url: ' + url

  req.pipe(request(url: url, method: req.method)).pipe(res)

# needed to provide compiled coffeescript files
app.get /^(\/[^\s]+)\.js$/, (req, res, next) ->
  js(req.params[0])
  next()

# needed to provide compiled less files
app.get /^(\/[^\s]+)\.css$/, (req, res, next) ->
  css(req.params[0])
  next()

module.exports = app

app.listen config.SERVER_PORT
console.log "server started on port " + config.SERVER_PORT
