exports.CLIENT_DIR = __dirname + '/../public'

db_user = ""
db_password = ""
db_domain = "127.0.0.1:5984"

exports.DB_DOMAIN = db_domain

db_url = "http://"
db_url += db_user if db_user
db_url += ":" + db_password if db_password
db_url += "@" if db_user
db_url += db_domain

exports.DB_URL = db_url

exports.SERVER_PORT = process.env.VMC_APP_PORT || 7070
