ddoc =
  _id: "_design/views"

  validate_doc_update: (newDoc, oldDoc, userCtx) ->
    if newDoc.type isnt 'comment' and userCtx.roles.indexOf('guest') >= 0
      throw forbidden: 'updating data is not allowed for type ' + newDoc.type

  views: {}

ddoc.views.draftsByLastChange =
  map: (doc) ->
    emit doc.lastChange, doc if doc.type is 'draft' and doc.lastChange

ddoc.views.draftsByID =
  map: (doc) ->
    emit doc._id, doc if doc.type is 'draft'

ddoc.views.menu =
  map: (doc) ->
    if doc.type is 'draft' and doc.active
      emit doc.lastChange,
        id: doc._id
        name: doc.name
        thumb: doc.thumbname
        date: doc.lastChange

ddoc.views.comments =
  map: (doc) ->
    if doc.type is 'comment' and doc.active
      emit [doc.refId, doc.timestamp],
        author: doc.author
        url: doc.url
        message: doc.message
        timestamp: doc.timestamp

ddoc.views.commentsAll =
  map: (doc) ->
    if doc.type is 'comment'
      emit [doc.refId, doc.timestamp], doc

module.exports = ddoc
